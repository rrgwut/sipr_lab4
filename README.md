# SiPR – ćwiczenie 4 – Planowanie ruchu manipulatora z wykorzystaniem MoveIt

## Przygotowanie środowiska pracy

Instrukcja przygotowania środowiska pracy zakłada, że wykonane zostały ćwiczenia:

  * [Przygotowanie środowiska ROS](https://bitbucket.org/rrgwut/sipr_lab0),
  * [Kinematyka i sterowanie pozycyjne manipulatora](https://bitbucket.org/rrgwut/sipr_lab3).

### Instalacja brakujących pakietów

Przed instalacją nowych pakietów z repozytorium Ubuntu należy zainstalować ewentualne aktualizacje. W konsoli `yakuake` (`F12`) wykonujemy polecenia:

```bash
sudo apt update
```

```bash
sudo apt upgrade
```

Instalacja brakujących pakietów ROS-a z repozytorium Ubuntu (Ubuntu 20.04, ROS noetic) wymaga uruchomienia w konsoli następujących poleceń (wciśnij `F12`, aby otworzyć terminal `yakuake`):

```bash
sudo apt install -y ros-noetic-rviz-visual-tools ros-noetic-moveit-visual-tools ros-noetic-franka-description
```


### Przygotowanie przestrzeni roboczej

Wchodzimy do podkatalogu `src` przestrzeni roboczej `sipr_ws`:

```
cd ~/sipr_ws/src
```

Ściągamy kod źródłowy pakietu `sipr_lab4` przygotowanego jako szablon do dalszego wykonania instrukcji:

```
git clone https://bitbucket.org/rrgwut/sipr_lab4
```

oraz 

```bash
git clone https://github.com/ros-planning/panda_moveit_config.git -b noetic-devel
```

Wywołujemy budowanie przestrzeni roboczej `sipr_ws`:

```
cd ~/sipr_ws
```

```
catkin_make -DCMAKE_BUILD_TYPE=Debug
```

### Uruchomienie środowiska VS Code

Uruchamiamy VS Code w przestrzeni roboczej `sipr_ws`:

```
code ~/sipr_ws
```

W VS Code zobaczymy zawartość całej przestrzeni roboczej `sipr_ws` w tym otwarte pliki z poprzednich ćwiczeń, które można zamknąć. W drzewie katalogów można zwinąć katalogi poprzednich ćwiczeń i rozwinąć katalog `sipr_lab4`, w którym znajdziemy tą instrukcję w pliku [`README.md`](./README.md).


#### Konfiguracji debuggera w VS Code

W VS Code należy wcisnąć skrót przycisk `Ctrl+Shift+D`, który przeniesienie nas z widoku drzewa katalogów do widoku `Uruchamiania`.

1. Jeżeli debugger **nie był jeszcze konfigurowany** należy wybrać opcję `create a launch.json file`, a dalej opcję `C++ (GDB/LLDB)`.
2. Jeżeli debugger **był już konfigurowany** należy wcisnąć przycisk `Open launch.json` (ten z trybikiem), a następnie w otwartym pliku [`sipr_ws/.vscode/launch.json`](../../.vscode/launch.json) kliknąć przycisk `Add configuration...`, który pojawia się w dolnym prawym rogu okna edycji, a następnie wybieramy `C/C++: (gdb) Launch`.

W pliku [`sipr_ws/.vscode/launch.json`](../../.vscode/launch.json) zmieniamy następujące linie:

```
...
            "name": "(gdb) sipr_lab4_pick_and_place",
...
            "program": "${workspaceFolder}/devel/lib/sipr_lab4/sipr_lab4_pick_and_place",
...
            "cwd": "${workspaceFolder}",
...
```

## Wykonanie ćwiczenia

W tym ćwiczeniu będzie możliwość zapoznania się ze środowiskiem `MoveIt` przeznaczonym do tworzenia zaawansowanych aplikacji dla manipulatorów z wykorzystaniem algorytmów planowania ruchu. 


**Do wykonania są następujące zadania:**

1. Przygotowanie pliku URDF dla robota Fanuc M10iA z zamontowanym chwytakiem.
2. Wygenerowanie pakietu z konfiguracją robota dla MoveIt
3. Zapoznanie się z możliwościami oprogramowania MoveIt + rviz + OMPL.
4. Uruchomienie i modyfikacja węzła demonstrującego możliwości MoveIt w realizacji zadania typu _pick-and-place_ z poziomu C++. 

### 1. Przygotowanie pliku URDF robota z chwytakiem

Pliki URDF pozwalają na zdefiniowanie modelu robota, zarówno geometrii, kinematyki, zakresów ruchu w osiach, jak i dynamiki.

W ćwiczeniu wykorzystamy gotowy model robota Fanuc M10iA zdefiniowany w pliku [`sipr_ws/src/fanuc/fanuc_m10ia_support/urdf/m10ia_macro.xacro`](../fanuc/fanuc_m10ia_support/urdf/m10ia_macro.xacro). Należy zwrócić uwagę, że plik ten ma rozszerzenie `.xacro`, a nie `.urdf`. 

Pliki `.xacro` są plikami o składni xml-owej pozwalającej zdefiniowanie tagów typowych dla `.urdf`, ale dodatkowo można w nich definiować makra, korzystać ze zmiennych, a nawet określać pewne parametry poprzez działania arytmetyczne. Oznacza to, że możliwe jest stworzenie uogólnionego modelu stanowiącego podstawę do wygenerowania końcowych plików `.urdf` z odmiennymi parametrami. 

Docelowe pliki `.urdf` są generowane z wykorzystaniem programu `xacro` dostarczanego w jednym z pakietów ROS-a. 

W ćwiczeniu wykorzystamy fakt, że pliki `.xacro` z innych pakietów ROS-a można załączać (importować) w kolejnych plikach `.xacro`. Dzięki temu w prostu sposób stworzymy definicję modelu robota Fanuc M10iA z chwytakiem zdefiniowanym w pakiecie `franka_description`. 

W tym celu należy utworzyć plik [`sipr_ws/src/sipr_lab4/urdf/fanuc_m10ia_with_hand.urdf.xacro`](./urdf/fanuc_m10ia_with_hand.xacro) i umieścić w nim następujący kod:

```xml
<?xml version="1.0" encoding="utf-8"?>
<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="fanuc_m10ia_with_hand">

  <xacro:include filename="$(find fanuc_m10ia_support)/urdf/m10ia_macro.xacro"/>
  <xacro:fanuc_m10ia prefix=""/>
  
  <xacro:include filename="$(find franka_description)/robots/common/utils.xacro" />
  <xacro:include filename="$(find franka_description)/robots/common/franka_hand.xacro"/>
  <xacro:franka_hand arm_id="" safety_distance="0.005" xyz="0.02 0 0" rpy="0 ${pi/2} 0" connected_to="link_6"/>

</robot>
```

W powyższym kodzie model ramienia jest zaimportowany bez dodatkowych parametrów. Natomiast chwytak jest odsunięty od sprzęgu o `0.02 [m]` oraz obrócony o `90` stopni wokół własnej osi `Y`. 

### 2. Wygenerowanie pakietu z konfiguracją robota dla MoveIt

`MoveIt` jest oprogramowaniem dość rozbudowanym i w znacznym stopniu bazującym na pakietach ROS-a. Aby w pełni wykorzystać jego możliwości należy przygotować szereg plików konfiguracyjnych i startowych, m.in.: skrypty startowe `.launch`, konfigurację programu `rviz`, konfiguracje rzeczywistych lub wirtualnych sterowników napędów, macierz samokolizji, definicje grup złączy robota nadającymi się do sterowania, itd. Na szczęście nie trzeba tego robić ręcznie. 

Razem z `MoveIt` dostarczany jest graficzny generator konfiguracji `MoveIt Setup Assistant`, który na podstawie pliku `.xacro` pomaga wygenerować docelowy pakiet. 

#### Uruchomienie `MoveIt Setup Assistant`

W celu uruchomienia programu `MoveIt Setup Assistant` należy w konsoli, np. `yakuake` (wciśnij `F12`) wywołać polecenie:

```bash
roslaunch moveit_setup_assistant setup_assistant.launch
```

![image](docs/figures/moveit_setup_assistant.png)

#### Załadowanie modelu robota

Należy nacisnąć `Create New MoveIt Configuration Package` i następnie wskazać ścieżkę do wcześniej wygenerowanego pliku `.xacro`: 

![image](docs/figures/msa_start.png)

... a następnie wcisnąć `Load Files`. 

Jeżeli plik `.xacro` jest poprawny, to w oknie podglądu zobaczymy model 3D robota. 

![image](docs/figures/msa_start_vis.png)

#### Generowanie macierzy samokolizji

Z menu po lewej stronie wybieramy `Self-Collisions` i klikamy `Generate Collision Matrix`. 

Po chwili pojawi się lista członów robota, dla których można pominąć sprawdzanie kolizji oraz uzasadnienie tego. Nie ma potrzeby sprawdzać kolizji między członami stanowiącymi parę ruchomą. Nie ma również potrzeby sprawdzania kolizji między członami, które nie mają możliwości nigdy się zetknąć, i w końcu nie ma potrzeby sprawdzania kolizji między członami, które stykają się z definicji (np. palce chwytaka). 

Warto przełączyć widok sprawdzenia samokolizji na macierz, co ułatwia interpretację poprawności modelu. 

![image](docs/figures/msa_self-collision.png)

#### Wirtualne złącza

W zakładce `Virtual Joints` można dodać wirtualne złącza. Zwyczajowo dodaje się nieruchome złącze między bazą robota `base_link` a globalnym układem współrzędnych `world`.  

Należy wcisnąć `Add Virtual Joint` i wprowadzić dane jako rysunku poniżej:

![image](docs/figures/msa_virtual_joints.png)


#### Definiowanie grup par kinematycznych

Na pełen model robota składa się nie tylko sześć członów ruchomych, ale również nieruchoma baza oraz efektor. 

Algorytmy planowania ruchu dostępne w `MoveIt` są w stanie zrealizować planowanie uwzględniając wszystkie ruchome człony, włącznie z palcami chwytaka. Jednak w praktyce, w celu zmniejszenia rozmiaru przestrzeni przeszukiwań, chwytak traktowany jest jako jedna bryła. Czerwony obszar na rysunku poniżej demonstruje obszar kolizyjny obejmujący pełen zakres ruchu palców, a więc chwytak otwarty jak i zamknięty traktowany jest tak samo przy sprawdzaniu kolizji.

![image](docs/figures/msa_show_collision_3d.png)

W `MoveIt` efektory z założenia są rozpatrywane osobno. Dlatego, w zakładce `Planning Groups`, zdefiniujemy dwie osobne grupy dla ramienia i dla chwytaka. 

##### Definiowanie grupy `manipulator`

Najpierw stworzymy grupę o nazwie `manipulator`. W tym celu należy wcisnąć przycisk `Add Group`, a następnie uzupełnić pola `Group Name` i `Kinematic Solver` jak poniżej. 

![image](docs/figures/msa_planning_group_manipulator.png)

---

W polu `Kinematic Solver` należy wybrać `kdl_kinematic_plugin/KDLKinematicsPlugin`, który jest uniwersalnym algorytmem iteracyjnym. 

Na rozwijanej liście można zauważyć, że dostępny jest również solver w postaci jawnej wygenerowany za pomocą IKFast (`fanuc_m10ia_manipulator_kinematics/IKFastKinematicsPlugin`). Niestety plugin ten nie jest kompatybilny z wersją `MoveIt` używaną w ćwiczeniu.

---

Następnie należy kliknąć `Add Joints` i zaznaczyć oraz przenieść na listę `Selected Joints` złącza od `joint_1` do `joint_6` (jak na rysunku poniżej). Listę zatwierdzamy przyciskiem `Save`.

![image](docs/figures/msa_manipulator_group_joints.png)


##### Definiowanie grupy `hand`

Grupę członów składających się na chwytak nazwiemy `hand`. Podobnie jak poprzednio klikamy przycisk `Add Group`. w polu `Group Name` wprowadzamy nazwę `hand`. Pozostałe pola pozostawiamy bez zmian (również pole `Kinematic Solver` pozostawiamy ustawione na `None`).

![image](docs/figures/msa_planning_group_hand.png) 

**Uwaga!** Tym razem klikamy `Add Links`, a nie `Add Joints` jak poprzednio. Wybieramy człony jak na rysunku poniżej i klikamy `Save`.

![image](docs/figures/msa_hand_group_links.png) 

Ostatecznie zakładka `Planning Groups` powinna wyglądać jak na rysunku poniżej.

![image](docs/figures/msa_planning_groups.png) 

#### Definiowanie typowych pozycji

W zakładce `Robot Poses` można zdefiniować typowe pozycje, do których będzie można odwoływać się po nazwie. 

Po kliknięciu `Add Pose` możemy ustawić pozycję każdego ze złączy, która to pozycja jest automatycznie wizualizowana w podglądzie. Na rysunku poniżej zdefiniowana została pozycja o nazwie `home` z wszystkimi wartościami położeń złączy ustawionymi na zero. 

![image](docs/figures/msa_home_position.png) 


#### Definiowanie efektora

W zakładce `End Effectors` klikamy przycisk `Add End Effector`. Ustawiamy pola jak na rysunku poniżej.

![image](docs/figures/msa_end_effector.png) 

Istotne jest aby poprawnie ustawić pola `End Effector Group` i `Parent Link`.

#### Wybór kontrolera złączy ruchomych

W zakładce `ROS Control` wciskamy przycisk `Add Controller`. 

Pola `Controller Name` i `Controller Type` ustawiamy jak na rysunku poniżej:

![image](docs/figures/msa_position_controller.png) 

Następnie klikamy `Add Planning Group Joints` i wybieramy grupę `manipulator`, jak poniżej:

![image](docs/figures/msa_position_controller_2.png) 

#### Informacje o autorze

Przechodzimy do zakładki `Author Information`. 

`MoveIt Setup Assistant` nie pozwoli na wygenerowanie pakietu konfiguracyjnego bez podania danych o autorze. W polach `Name ...` i `Email ...` można podać odpowiednio:
  * `sipr`
  * `sipr@pw.edu.pl`

Ważne jest, żeby w polu `Email` podać tekst wyglądający na poprawny adres email.

#### Generowanie pakietu konfiguracyjnego

Ostatnim krokiem jest wygenerowanie pakietu z plikami konfiguracyjnymi. 

Przechodzimy do zakładki `Configuration Files`. 

Jedyną, ale istotną, informacją jaka musimy podać jest nazwa katalogu, w którym zostanie umieszczony nowy pakiet. W polu `Configuration Package Save Path` wpisujemy ścieżkę `/home/sipr/sipr_ws/src/fanuc_m10ia_with_hand_config`.

Klikamy `Generate Package`, co kończy cały proces.

![image](docs/figures/msa_generate_package.png) 

Zamykamy `MoveIt Setup Assistant`'a.

Z poziomu VS Code możemy zauważyć, że w katalogu `sipr_ws/src/` pojawił się nowy pakiet. 

**Należy teraz przebudować całą przestrzeń roboczą wywołując `catkin_make`'a:** 

1. z poziomu konsoli
```bash
cd ~/sipr_ws
catkin_make -DCMAKE_BUILD_TYPE=Debug
```

2. lub z poziomu VS Code wciskając `Ctrl+Shift+B`. 


### 3. Zapoznanie się z możliwościami oprogramowania MoveIt + rviz + OMPL

W konsoli wywołujemy polecenie:

```bash
roslaunch fanuc_m10ia_with_hand_config demo.launch
```

Powinniśmy zobaczyć okno `rviz`'a ze wstępnie skonfigurowanym widokiem i plugin'ami.

![image](docs/figures/rviz_demo_1.png) 

Oprócz `rviz`'a skrypt startowy `demo.launch` uruchamia również inne węzły, z których centralną rolę odgrywa węzeł `move_group`, co można zaobserwować w programie `rqt_graph` (rysunek poniżej). 

![image](docs/figures/rqt_graph_demo.png) 

Węzeł `move_group` pełni tutaj również rolę atrapy sterownika pozycji (wysyła dane na kanale `/move_group/fake_controller_joint_states`), który przy pracy z rzeczywistym robotem byłby odrębnym węzłem. 

#### Planowanie ruchu

Dzięki plugin'om z poziomu `rviz`'a możemy uruchomić planowanie ruchu i wykonanie wygenerowanej ścieżki. 

Najpierw należy ustawić, że planowanie ma się odbywać dla grupy `manipulator`. W drzewie wyświetlanych obiektów `Display: Motion Planning > Planning Request`) należy odnaleźć pole `Planning Group` i z rozwijanej listy wybrać `manipulator`. 

![image](docs/figures/rviz_demo_2.png) 

W okolicach końcówki roboczej pojawią uchwyty – które można przeciągać myszką – pozwalające na zdefiniowanie docelowej pozycji narzędzia. 

![image](docs/figures/rviz_demo_drag_goal.png) 

Uchwyty pozwalają na przesunięcia i obroty we wszystkich kierunkach. 

Po zakończeniu ustawiania pozycji docelowej widzimy robota w obu pozycjach, aktualnej i docelowej. 

![image](docs/figures/rviz_demo_goal.png) 

Po wciśnięciu przycisku `Plan` z panelu `Motion Planning` po lewej stronie, przy założeniu, że plan został odnaleziony, powinna uruchomić się animacja ścieżki jaką pokona robot. 

![image](docs/figures/rviz_demo_anim.png) 

**Należy mieć na uwadze, że jest to tylko animacja. Robot nie realizuje w rzeczywistości ruchu. Nawet na poziomie atrapy kontrolera.**. 

Aby zaplanowania ścieżka została zrealizowana, należy wcisnąć przycisk `Execute`. 

Po zakończeniu ruchu, pozycja aktualna i zadana będą się pokrywać w wizualizacji. 

![image](docs/figures/rviz_demo_execute.png) 

W panelu `Motion Planning`, w zakładce `Scene Objects` mamy możliwość ręcznego dodawania obiektów do sceny. 

![image](docs/figures/rviz_demo_scene_object.png) 


**Zadanie do wykonania:**

W ramach ćwiczeń należy:

  * przygotować scenę podobną do prezentowanej na rysunku poniżej (przestrzeń między sześcianami jest wystarczająco duża, aby zmieścił się tam robot), 
  * doprowadzić robota do pozycji po jednej stronie przeszkody,
  * zdefiniować pozycję docelową po drugiej stronie przeszkody, 
  * przetestować działanie algorytmu planowania (wystarczy uruchamiać samo `Plan` bez `Execute`).


![image](docs/figures/rviz_demo_planning_scene.png) 

Testowanie planowania należy powtórzyć kilkukrotnie dla kilku wybranych algorytmów dostarczanych przez bibliotekę OMPL. Wyboru algorytmu można dokonać w zakładce `Planning Context` (rozwijana lista pod napisem OMPL): 

![image](docs/figures/rviz_demo_planning_context.png) 

W szczególności należy sprawdzić algorytmy:

 * PRM
 * RRT
 * RRTConnect
 * RRTstar
 * BKPIECE
 

---

**Przed przystąpieniem do następnego punktu należy zamknąć wcześniej uruchomiony skrypt `fanuc_m10ia_with_hand_config demo.launch`.**

---

### 4. Uruchomienie i modyfikacja węzła demonstrującego możliwości MoveIt w realizacji zadania typu _pick-and-place_ z poziomu C++. 

W tym punkcie ćwiczenia wykorzystamy inny skrypt startowy `roslaunch panda_moveit_config demo.launch` oraz wstępnie zaimplementowany węzeł `sipr_lab4_pick_and_place` demonstrujący aplikację typu pick and place. 

Najpierw uruchomimy wizualizację. W terminalu `yakuake` (`F12`) włączamy skrypt `roslaunch`'a

```bash
roslaunch panda_moveit_config demo.launch
```

Następnie z poziomu VS Code uruchamiamy węzeł `sipr_lab4_pick_and_place`. W tym celu w widoku uruchamiania (`Ctrl+Shift+D`) na liście rozwijanej wybieramy opcję `(gdb) sipr_lab4_pick_and_place` i wciskamy `F5` lub przycisk z zieloną strzałką.

<!-- Po uruchomieniu węzła w konsoli pojawi się tekst:

    Waiting to continue: Press 'next' in the RvizVisualToolsGui window to continue the demo

Należy przejść do okna RViza i wcisnąć Next na panelu w lewym dolnym rogu.  -->

![image](docs/figures/rviz_next.png) 

<!-- Dodatkowe komunikaty pojawiają się jako tekst w wizualizacji 3D.  -->

Program realizuje następujące kroki:

<!-- 1. Dojazd do pozycji `home` zdefiniowanej wcześniej w MoveIt Setup Assistant.  -->
1. Chwyt i podniesienie obiektu. 
1. Odstawienie obiektu w nowe miejsce.
<!-- 1. Dojazd do pozycji zakodowanej na stałe w kodzie węzła `sipr_lab4_pick_and_place`. -->

**Zadanie do wykonania**

Należy przeanalizować kod źródłowy i przeczytać komentarze w pliku [`src/sipr_lab4/src/sipr_lab4_pick_and_place.cpp`](./src/sipr_lab4_pick_and_place.cpp). 

Następnie należy zmodyfikować program tak, aby działał dla 3 klocków ustawionych w rzędzie (zmodyfikować program, tak aby pozycje klocków były wyliczane automatycznie).

---

**Podpowiedzi:** 

  * W przypadku problemów z planowaniem przy większej liczbie obiektów, na panelu `MotionPlanning > Context` można spróbować zmienić algorytm planowania. Jako domyślny jest zazwyczaj używany algorytm BKPIECE.
  * Pozycje chwytu i odłożenia są wizualizowane w RViz.  
  * **Nazwy obiektów muszą być unikalne.**

---
